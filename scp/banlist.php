<?php
/*********************************************************************
    banlist.php

    List of banned email addresses

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('admin.inc.php');
include_once(INCLUDE_DIR.'class.banlist.php');

/* Get the system ban list filter */
if(!($filter=Banlist::getFilter()))
    $warn = 'La liste noire du sytème est vide.';
elseif(!$filter->isActive())
    $warn = 'Le filtre de la LISTE NOIRE DU SYSTEME EST <b>DÉSACTIVÉ</b> - <a href="filters.php">l\'activer ici</a>.';

$rule=null; //ban rule obj.
if($filter && $_REQUEST['id'] && !($rule=$filter->getRule($_REQUEST['id'])))
    $errors['err'] = 'Liste d\'identifiants incorrects ou inconnus';

if($_POST && !$errors && $filter){
    switch(strtolower($_POST['do'])){
        case 'update':
            if(!$rule){
                $errors['err']='Règle d\'exclusion inconnue ou invalide.';
            }elseif(!$_POST['val'] || !Validator::is_email($_POST['val'])){
                $errors['err']=$errors['val']='Adresse e-mail valide obligatoire';
            }elseif(!$errors){
                $vars=array('w'=>'email',
                            'h'=>'equal',
                            'v'=>trim($_POST['val']),
                            'filter_id'=>$filter->getId(),
                            'isactive'=>$_POST['isactive'],
                            'notes'=>$_POST['notes']);
                if($rule->update($vars,$errors)){
                    $msg='Email mis à jour correctement';
                }elseif(!$errors['err']){
                    $errors['err']='Erreur de la mise à jour de la règle d\'exclusion. Essayez encore !';
                }
            }
            break;
        case 'add':
            if(!$filter) {
                $errors['err']='Règle d\'exclusion inconnue ou invalide';
            }elseif(!$_POST['val'] || !Validator::is_email($_POST['val'])) {
                $errors['err']=$errors['val']='Adresse e-mail valide obligatoire';
            }elseif(BanList::includes(trim($_POST['val']))) {
                $errors['err']=$errors['val']='Email déjà présent dans la règle d\'exclusion';
            }elseif($filter->addRule('email','equal',trim($_POST['val']),array('isactive'=>$_POST['isactive'],'notes'=>$_POST['notes']))) {
                $msg='Email correctement ajouté à la règle d\'exclusion';
                $_REQUEST['a']=null;
                //Add filter rule here.
            }elseif(!$errors['err']){
                $errors['err']='Erreur de création de la règle d\'exclusion. Essayez encore !';
            }
            break;
        case 'mass_process':
            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err'] = 'Vous devez sélectionner au moins un email pour continuer.';
            } else {
                $count=count($_POST['ids']);
                switch(strtolower($_POST['a'])) {
                    case 'enable':
                        $sql='UPDATE '.FILTER_RULE_TABLE.' SET isactive=1 '
                            .' WHERE filter_id='.db_input($filter->getId())
                            .' AND id IN ('.implode(',', db_input($_POST['ids'])).')';
                        if(db_query($sql) && ($num=db_affected_rows())){
                            if($num==$count)
                                $msg = 'Statut d\'exclusion des emails sélectionnés passés en activés';
                            else
                                $warn = "$num des $count emails sélectionnés ont le statut d\'exclusion activé";
                        } else  {
                            $errors['err'] = 'Impossible d\'activer les emails sélectionnés';
                        }
                        break;
                    case 'disable':
                        $sql='UPDATE '.FILTER_RULE_TABLE.' SET isactive=0 '
                            .' WHERE filter_id='.db_input($filter->getId())
                            .' AND id IN ('.implode(',', db_input($_POST['ids'])).')';
                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Statut d\'exclusion des emails sélectionnés passés en désactivés';
                            else
                                $warn = "$num des $count emails sélectionnés passés en statut d\'exclusion désactivé";
                        } else {
                            $errors['err'] = 'Impossible de désactiver les emails sélectionnés';
                        }
                        break;
                    case 'delete':
                        $i=0;
                        foreach($_POST['ids'] as $k=>$v) {
                            if(($r=FilterRule::lookup($v)) && $r->getFilterId()==$filter->getId() && $r->delete())
                                $i++;
                        }
                        if($i && $i==$count)
                            $msg = 'Emails sélectionnés supprimés correctement de la règle d\'exclusion';
                        elseif($i>0)
                            $warn = "$i des $count emails sélectionnés supprimés de la règle d'exclusion";
                        elseif(!$errors['err'])
                            $errors['err'] = 'Impossible de supprimer les emails sélectionnés';

                        break;
                    default:
                        $errors['err'] = 'Action inconnue - consulter l\'aide technique';
                }
            }
            break;
        default:
            $errors['err']='Action inconnue';
            break;
    }
}

$page='banlist.inc.php';
$tip_namespace = 'emails.banlist';
if(!$filter || ($rule || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add')))) {
    $page='banrule.inc.php';
}

$nav->setTabActive('emails');
$ost->addExtraHeader('<meta name="tip-namespace" content="' . $tip_namespace . '" />',
    "$('#content').data('tipNamespace', '".$tip_namespace."');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
