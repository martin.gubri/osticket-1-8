<?php
/*********************************************************************
    profile.php

    Staff's profile handle

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

require_once('staff.inc.php');
$msg='';
$staff=Staff::lookup($thisstaff->getId());
if($_POST && $_POST['id']!=$thisstaff->getId()) { //Check dummy ID used on the form.
 $errors['err']='Erreur interne. Action refusée';
} elseif(!$errors && $_POST) { //Handle post

    if(!$staff)
        $errors['err']='Personnel inconnu ou invalide';
    elseif($staff->updateProfile($_POST,$errors)){
        $msg='Profil mis à jour avec succès';
        $thisstaff->reload();
        $staff->reload();
        $_SESSION['TZ_OFFSET']=$thisstaff->getTZoffset();
        $_SESSION['TZ_DST']=$thisstaff->observeDaylight();
    }elseif(!$errors['err'])
        $errors['err']='Erreur lors de la mise à jour du profil. Essayez de corriger les erreurs ci-dessous et recommencez !';
}

//Forced password Change.
if($thisstaff->forcePasswdChange() && !$errors['err'])
    $errors['err']=sprintf('<b>Bonjour %s</b> - Vous devez changer votre mot de passe pour continuer !',$thisstaff->getFirstName());
elseif($thisstaff->onVacation() && !$warn)
    $warn=sprintf('<b>Heureux de vous revoir %s</b> ! Vous êtes listés \'en vacances\' Prévenez votre responsable que vous êtes de retour.',$thisstaff->getFirstName());

$inc='profile.inc.php';
$nav->setTabActive('dashboard');
$ost->addExtraHeader('<meta name="tip-namespace" content="dashboard.my_profile" />',
    "$('#content').data('tipNamespace', 'dashboard.my_profile');");
require_once(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$inc);
require_once(STAFFINC_DIR.'footer.inc.php');
?>
