<?php
/*********************************************************************
    settings.php

    Handles all admin settings.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('admin.inc.php');
$errors=array();
$settingOptions=array(
    'system' =>
        array('Configuration du système', 'settings.system'),
    'tickets' =>
        array('Configuration et options des tickets', 'settings.ticket'),
    'emails' =>
        array('Configuration email', 'settings.email'),
    'pages' =>
        array('Pages du site', 'settings.pages'),
    'access' =>
        array('Controle des accès', 'settings.access'),
    'kb' =>
        array('Configuration de la base de connaissances', 'settings.kb'),
    'autoresp' =>
        array('Configuration de la réponse automatique', 'settings.autoresponder'),
    'alerts' =>
        array('Configuration des alertes et des notifications', 'settings.alerts'),
);
//Handle a POST.
$target=($_REQUEST['t'] && $settingOptions[$_REQUEST['t']])?$_REQUEST['t']:'system';
$page = false;
if (isset($settingOptions[$target]))
    $page = $settingOptions[$target];

if($page && $_POST && !$errors) {
    if($cfg && $cfg->updateSettings($_POST,$errors)) {
        $msg=Format::htmlchars($page[0]).' Mis à jour avec succès';
    } elseif(!$errors['err']) {
        $errors['err']='Impossible de mettre à jour la configuration - corrigez les erreurs ci-dessous et essayez à nouveau';
    }
}

$config=($errors && $_POST)?Format::input($_POST):Format::htmlchars($cfg->getConfigInfo());
$ost->addExtraHeader('<meta name="tip-namespace" content="'.$page[1].'" />',
    "$('#content').data('tipNamespace', '".$page[1]."');");

$nav->setTabActive('settings', ('settings.php?t='.$target));
require_once(STAFFINC_DIR.'header.inc.php');
include_once(STAFFINC_DIR."settings-$target.inc.php");
include_once(STAFFINC_DIR.'footer.inc.php');
?>
