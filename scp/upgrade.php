<?php
/*********************************************************************
    upgrade.php

    osTicket Upgrade Wizard

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require_once 'admin.inc.php';
require_once INCLUDE_DIR.'class.upgrader.php';

//$_SESSION['ost_upgrader']=null;
$upgrader = new Upgrader(TABLE_PREFIX, UPGRADE_DIR.'streams/');
$errors=array();
if($_POST && $_POST['s'] && !$upgrader->isAborted()) {
    switch(strtolower($_POST['s'])) {
        case 'prereq':
            if(!$ost->isUpgradePending()) {
                $errors['err']=' Rien à faire ! Le système est déjà à jour dans sa dernière version';
            } elseif(!$upgrader->isUpgradable()) {
                $errors['err']='Le système de mise à jour ne supporte PAS la mise à jour depuis la version actuelle !';
            } elseif(!$upgrader->check_prereq()) {
                $errors['prereq']='Les prérequis minimum ne sont pas satisfaits ! Se référer aux notes de version pour plus d\'informations';
            } elseif(!strcasecmp(basename(CONFIG_FILE), 'settings.php')) {
                $errors['err']='Le renommage du fichier de configuration est nécessaire pour continuer !';
            } else {
                $upgrader->setState('upgrade');
            }
            break;
        case 'upgrade': //Manual upgrade.... when JS (ajax) is not supported.
            if($upgrader->getTask()) {
                $upgrader->doTask();
            } elseif($ost->isUpgradePending() && $upgrader->isUpgradable()) {
                $upgrader->upgrade();
            } elseif(!$ost->isUpgradePending()) {
                $upgrader->setState('done');
            }

            if(($errors=$upgrader->getErrors()))  {
                $upgrader->setState('aborted');
            }
            break;
        default:
            $errors['err']='Action inconnue !';
    }
}

switch(strtolower($upgrader->getState())) {
    case 'aborted':
        $inc='aborted.inc.php';
        break;
    case 'upgrade':
        $inc='upgrade.inc.php';
        break;
    case 'done':
        $inc='done.inc.php';
        break;
    default:
        $inc='prereq.inc.php';
        if($upgrader->isAborted())
            $inc='aborted.inc.php';
        elseif(!strcasecmp(basename(CONFIG_FILE), 'settings.php'))
            $inc='rename.inc.php';
        elseif(!$ost->isUpgradePending())
            $errors['err']='Rien à faire ! Système déjà à jour à la version <b>'.$ost->getVersion().'</b> avec aucun patch à appliquer.';
        elseif(!$upgrader->isUpgradable())
            $errors['err']=sprintf('Le système de mise à jour ne supporte PAS la mise à jour depuis le patch actuel [%s] !', $cfg->getSchemaSignature());

}

$nav = new AdminNav($thisstaff);
$nav->setTabActive('dashboard');
$nav->addSubMenu(array('desc'=>'Upgrader',
                           'title'=>'Upgrader',
                           'href'=>'upgrade.php',
                           'iconclass'=>'preferences'),
                        true);
$ost->addExtraHeader('<script type="text/javascript" src="./js/upgrader.js"></script>');
require(STAFFINC_DIR.'header.inc.php');
require(UPGRADE_DIR.$inc);
require(STAFFINC_DIR.'footer.inc.php');
?>
